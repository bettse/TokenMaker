//
//  USBPortal.swift
//  TokenMaker
//
//  Created by Eric Betts on 6/1/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Foundation
import IOKit.hid

class USBPortal : Portal {
    static let singleton = USBPortal()
    let vendorId = 0x1430
    let productId = 0x0150
    var reportSize : CFIndex = 0x20
    
    var portalThread : Thread?
    var device : IOHIDDevice? = nil
    var delegate : PortalDelegate? = nil
    
    func input(_ inResult: IOReturn, inSender: UnsafeMutableRawPointer, type: IOHIDReportType, reportId: UInt32, report: UnsafeMutablePointer<UInt8>, reportLength: CFIndex) {
        reportSize = reportLength
        let report = Data(bytes: UnsafePointer<UInt8>(report), count: reportLength)
        // print("USB IN: \(report.hexEncodedString())")
        self.delegate?.input(report)
    }
    
    func output(_ report: Data) {
        let reportId : CFIndex = 0
        if (report.count > reportSize) {
            print("output data too large for USB report")
            return
        }
        
        if let portal = device {
            print("USB OUT: \(report.hexEncodedString())")
            IOHIDDeviceSetReport(portal, kIOHIDReportTypeOutput, reportId, [UInt8](report), report.count)
        }
    }
    
    func connected(_ inResult: IOReturn, inSender: UnsafeMutableRawPointer, inIOHIDDeviceRef: IOHIDDevice!) {
        // It would be better to look up the report size and create a chunk of memory of that size
        let report = UnsafeMutablePointer<UInt8>.allocate(capacity: reportSize)
        device = inIOHIDDeviceRef
        
        IOHIDDeviceGetProperty(device!, kIOHIDMaxFeatureReportSizeKey as CFString)
        
        let 🙊 : IOHIDReportCallback = { inContext, inResult, inSender, type, reportId, report, reportLength in
            let this : USBPortal = Unmanaged<USBPortal>.fromOpaque(inContext!).takeUnretainedValue()
            this.input(inResult, inSender: inSender!, type: type, reportId: reportId, report: report, reportLength: reportLength)
        }
        
        //Hook up inputcallback
        let this = Unmanaged.passRetained(self).toOpaque()
        IOHIDDeviceRegisterInputReportCallback(device!, report, reportSize, 🙊, this)
        
        //Let the world know
        self.delegate?.deviceConnected(self)
    }
    
    func removed(_ inResult: IOReturn, inSender: UnsafeMutableRawPointer, inIOHIDDeviceRef: IOHIDDevice!) {
        self.delegate?.deviceDisconnected(self)
    }
    
    func discover() {
        self.portalThread = Thread(target: self, selector:#selector(start), object: nil)
        print("USBPortal discover")
        if let thread = portalThread {
            thread.start()
        }
    }
    
    @objc func start() {
        let deviceMatch = [kIOHIDProductIDKey: productId, kIOHIDVendorIDKey: vendorId ]
        let managerRef = IOHIDManagerCreate(kCFAllocatorDefault, IOOptionBits(kIOHIDOptionsTypeNone))
        
        IOHIDManagerSetDeviceMatching(managerRef, deviceMatch as CFDictionary)
        IOHIDManagerScheduleWithRunLoop(managerRef, CFRunLoopGetCurrent(), CFRunLoopMode.defaultMode.rawValue);
        IOHIDManagerOpen(managerRef, 0);
        
        let 🙈 : IOHIDDeviceCallback = { inContext, inResult, inSender, inIOHIDDeviceRef in
            let this : USBPortal = Unmanaged<USBPortal>.fromOpaque(inContext!).takeUnretainedValue()
            this.connected(inResult, inSender: inSender!, inIOHIDDeviceRef: inIOHIDDeviceRef)
        }
        
        let 🙉 : IOHIDDeviceCallback = { inContext, inResult, inSender, inIOHIDDeviceRef in
            let this : USBPortal = Unmanaged<USBPortal>.fromOpaque(inContext!).takeUnretainedValue()
            this.removed(inResult, inSender: inSender!, inIOHIDDeviceRef: inIOHIDDeviceRef)
        }
        
        IOHIDManagerRegisterDeviceMatchingCallback(managerRef, 🙈, unsafeBitCast(self, to: UnsafeMutableRawPointer.self))
        IOHIDManagerRegisterDeviceRemovalCallback(managerRef, 🙉, unsafeBitCast(self, to: UnsafeMutableRawPointer.self))
                
        RunLoop.current.run();
    }
}
