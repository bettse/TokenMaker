//
//  CustomWindow.swift
//  TokenMaker
//
//  Created by Eric Betts on 11/20/17.
//  Copyright © 2017 Eric Betts. All rights reserved.
//  Fixes "Unknown Window class (null) in Interface Builder file,
//  creating generic Window instead" error https://stackoverflow.com/a/47067809/1112230

import Cocoa

class CustomWindow : NSWindow {
}
