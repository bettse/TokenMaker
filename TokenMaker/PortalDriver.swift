//
//  PortalDriver.swift
//  TokenMaker
//
//  Created by Eric Betts on 6/1/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Foundation
import Cocoa

/*

 To make a physical token, place a factory default mifare classic on the reader first.  Then select a model and click 'build'
 To make a virtual token, click build with no token on portal or portal disconnected.
 To dump a real token.....TODO
 
*/

class PortalDriver : PortalDelegate {
    enum Key : UInt8 {
        case factory = 0x00
        case keygen = 0x01
    }
    static let singleton = PortalDriver()
    let HEX = 0x10
    let white : Data = Data(bytes: ["C".asciiValue, 0xFF, 0xFF, 0xFF])
    let red : Data = Data(bytes: ["C".asciiValue, 0xFF, 0x00, 0x00])
    let green : Data = Data(bytes: ["C".asciiValue, 0x00, 0xFF, 0x00])
    let blue : Data = Data(bytes: ["C".asciiValue, 0x00, 0x00, 0xFF])
    let purple : Data = Data(bytes: ["C".asciiValue, 0xFF, 0x00, 0xFF])
    let ready : Data = Data(bytes: ["L".asciiValue, 0x00, 0x80, 0x00, 0x00])
    let done : Data = Data(bytes: ["L".asciiValue, 0x00, 0xFF, 0x00, 0x01])
    var timer : Timer?
    let fileManager = FileManager.default
    
    var keyMode : Key = .keygen

    var downloadsDirectory: URL {
        get {
            return fileManager.urls(for: .downloadsDirectory, in: .userDomainMask).first!
        }
    }
    
    var portal : ProxyPortal = ProxyPortal()
    var next : (index: UInt8, token: SkylanderToken?) = (0, nil)
    
    init() {
        self.portal.delegate = self
    }
    
    func build(_ model: Model) {
        guard let token = self.next.token else {
            print("Could not find next token")
            return
        }
        token.model = model
        writeToken(self.next.index, block: 1)
    }
    
    func input(_ report: Data) {
        let command : UInt8 = report[0]
        let contents : Data = report.subdata(in: 1..<report.count)
        
        switch(command) {
        case "A".asciiValue:
            print("Activate ack \(contents.hexEncodedString())")
            portal.output(ready)
        case "K".asciiValue:
            print("Key configuration ack \(contents.hexEncodedString())")
        case "Q".asciiValue:
            //Check for error to determine if token is writable
            let index = contents[0] & 0x0f
            let error = contents[0] & 0xf0
            let block = contents[1]
            let blockData = contents.subdata(in: 2..<2+MifareClassic1K.blockSize)
            if (error == 0) {
                print("Error reading token, trying factory key mode")
                keyConfig(Key.factory)
            } else {
                if (block == 0) {
                    let uid = blockData.subdata(in: 0..<4)
                    print("Building blank for \(uid.hexEncodedString())")
                    self.next = (index, SkylanderToken.buildBlank(blockData))
                    portal.output(blue)
                }
                if (Int(block) < MifareClassic1K.blockCount) {
                    if let token = self.next.token {
                        token.load(block, blockData: blockData)
                    }
                }
                
                // Dumping real token
                if (keyMode == .keygen && Int(block) < MifareClassic1K.blockCount - 1) {
                    readToken(index, block: block+1)
                }
                
                if (Int(block) == MifareClassic1K.blockCount - 1) {
                    if let token = self.next.token {
                        let model = token.model
                        print("\(model.id) \(model.series) \(model.name) - (\(model.role))")
                        token.dump(URL(string:"file:///Users/bettse/Downloads/")!)
                    }
                }
            }
        case "R".asciiValue:
            print("Reset ack \(contents)")
        case "S".asciiValue:
            parseStatus(report.subdata(in: 1..<5))
        case "W".asciiValue:
            let index = contents[0] & 0x0f
            _ = contents[0] & 0xf0
            let block = contents[1]
                print("W ack \(index) block \(block)")
                switch(block) {
                case 0..<UInt8(MifareClassic1K.blockCount - 1):
                    writeToken(index, block: block+1)
                case UInt8(MifareClassic1K.blockCount - 1):
                    self.next = (0, nil)
                    portal.output(green)
                    portal.output(done)
                default:
                    print("Ack for writing block \(block)")
                }
        default:
            print("Unhandled comand code: \(command) \(contents)")
        }
    }
    
    //https://fgiesen.wordpress.com/2009/12/13/decoding-morton-codes/
    func Compact1By1(_ input : UInt32) -> UInt16  {
        var x : UInt32 = input
        x &= 0x55555555;                  // x = -f-e -d-c -b-a -9-8 -7-6 -5-4 -3-2 -1-0
        x = (x ^ (x >>  1)) & 0x33333333; // x = --fe --dc --ba --98 --76 --54 --32 --10
        x = (x ^ (x >>  2)) & 0x0f0f0f0f; // x = ---- fedc ---- ba98 ---- 7654 ---- 3210
        x = (x ^ (x >>  4)) & 0x00ff00ff; // x = ---- ---- fedc ba98 ---- ---- 7654 3210
        x = (x ^ (x >>  8)) & 0x0000ffff; // x = ---- ---- ---- ---- fedc ba98 7654 3210
        return UInt16(x);
    }
    
    /*
     Status is 16 pairs of bits
     high bit: token update bit
     low bit: token state bit
     
     00 = no token
     01 = token present
     10 = token left (present, update state to not present)
     11 = token entered (not present, update state to present)
     */
    func parseStatus(_ encodedStatus: Data) {
        let statusWord = encodedStatus.uint32
        let stateBits = Compact1By1(statusWord >> 0)
        let updateBits = Compact1By1(statusWord >> 1)
        if ((updateBits & stateBits) > 0) { //Arrival
            let index : UInt8 = UInt8(log2(Float(updateBits)))
            print("New token at index \(index)")
            readToken(index, block: 0)
        }
    }
    
    func readToken(_ index: UInt8, block: UInt8) {
        let readBlock = Data(bytes: ["Q".asciiValue, index, block] as [UInt8])
        print("Read [\(block)] @ \(index)")
        portal.output(readBlock)
    }
 
    func writeToken(_ index: UInt8, block: UInt8) {
        guard let token = self.next.token else {
            print("Could not find token for index \(index)")
            return
        }
        
        let blockData = token.block(block)
        var write = Data(bytes: ["W".asciiValue, index, block] as [UInt8])        
        write.append(blockData)
        print("Write [\(block)]: \(write.hexEncodedString())")
        portal.output(write)
    }
    
    func keyConfig(_ state : Key) {
        let keyConfig = Data(bytes: ["K".asciiValue, state.rawValue] as [UInt8])
        keyMode = state
        portal.output(keyConfig)
    }
    
    func deviceConnected(_ portal : Portal) {
        print("PortalDriver device connected")
        let activate = Data(bytes: ["A".asciiValue, 0x01])
        self.portal.output(activate)
        self.portal.output(ready)
        if #available(OSX 10.12, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
                let statusCommand = Data(bytes: ["S".asciiValue] as [UInt8])
                self.portal.output(statusCommand)
            }
        }
        keyConfig(Key.factory)
    }
    
    func deviceDisconnected(_ portal : Portal) {
        print("PortalDriver device disconnected")
        if let timer = timer {
            timer.invalidate()
        }        
    }
}
