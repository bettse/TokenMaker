//
//  NSData+toHex.swift
//  TokenMaker
//
//  Created by Eric Betts on 5/30/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Foundation

public extension Data {
    init(fromHex: String) {
        let hexArray = fromHex.trimmingCharacters(in: CharacterSet.whitespaces).components(separatedBy: " ")
        let hexBytes : [UInt8] = hexArray.map({UInt8($0, radix: 0x10)!})
        self = Data(bytes: hexBytes as [UInt8])
    }
    
    var toHex : String {
        return hexEncodedString()
    }
    
    
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
