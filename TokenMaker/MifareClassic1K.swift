//
//  MifareClassic1K.swift
//  TokenMaker
//
//  Created by Eric Betts on 5/30/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Foundation

enum NfcType : UInt8 {
    case none = 0
    case mifareClassic1K = 0x08
    case mifareMini = 0x09
    case mifareClassic4K = 0x18
    case desFire = 0x20
}

//MARK: - Equatable
func ==(lhs: MifareClassic1K, rhs: MifareClassic1K) -> Bool {
    return lhs.hashValue == rhs.hashValue
}

class MifareClassic1K : Hashable, CustomStringConvertible {
    static let sectorSize : Int = 4 //Blocks
    static let sectorCount : Int = 0x10
    static let blockCount : Int = sectorSize * sectorCount
    static let blockSize : Int = 0x10
    static let tokenSize : Int = blockSize * blockCount
    static let keyLength : Int = 6
    
    static let emptyBlock = Data(fromHex: "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00")
    static let rw_sector = Data(fromHex: "00 00 00 00 00 00 7F 0F 08 69 ff ff ff ff ff ff")
    static let ro_sector = Data(fromHex: "00 00 00 00 00 00 0F 0F 0F 69 ff ff ff ff ff ff")
    
    let emptyBlock : Data = MifareClassic1K.emptyBlock
    let ro_sector : Data = MifareClassic1K.ro_sector
    let rw_sector : Data = MifareClassic1K.rw_sector
    
    var nfcType : NfcType = .mifareClassic1K
    var uid : Data
    var data : NSMutableData = NSMutableData()
    
    var filename : String {
        get {
            return "\(uid.toHex).bin"
        }
    }
    
    var description: String {
        let me = String(describing: type(of: self)).components(separatedBy: ".").last!
        return "\(me)(\(uid.toHex))"
    }
    
    init(uid: Data) {
        self.uid = uid
        if (self.data.length < MifareClassic1K.blockSize) {
            self.data.append(emptyBlock)
        }
        self.data.replaceBytes(in: NSMakeRange(0, 4), withBytes: (uid as NSData).bytes)
    }
    
    convenience init(image: Data) {
        self.init(uid: image.subdata(in: 0..<3))
        self.data = NSMutableData.init(data: image)
    }
    
    convenience init(file: URL) {
        var image : Data = Data()
        do {
            image = try Data(contentsOf: file)
        } catch {
        }
        self.init(image: image)
    }
    
    //MARK: - Hashable
    var hashValue : Int {
        get {
            return uid.hashValue
        }
    }
    
    func nextBlock() -> Int {
        return data.length / MifareClassic1K.blockSize
    }
    
    func complete() -> Bool{
        return (nextBlock() == MifareClassic1K.blockCount)
    }
    
    func block(_ blockNumber: UInt8) -> Data {
        return block(Int(blockNumber))
    }
    
    func block(_ blockNumber: Int) -> Data {
        let blockStart = blockNumber * MifareClassic1K.blockSize
        let blockRange = NSMakeRange(blockStart, MifareClassic1K.blockSize)
        return data.subdata(with: blockRange)
    }
    
    func load(_ blockNumber: Int, blockData: Data) {
        if (blockNumber == nextBlock()) {
            data.append(blockData)
        } else {
            let blockRange = NSMakeRange(blockNumber * MifareClassic1K.blockSize, MifareClassic1K.blockSize)
            data.replaceBytes(in: blockRange, withBytes: (blockData as NSData).bytes)
        }        
    }
    
    func load(_ blockNumber: UInt8, blockData: Data) {
        load(Int(blockNumber), blockData: blockData)
    }
    
    func isSectorTrailer(_ blockNumber : Int) -> Bool {
        return (blockNumber + 1) % 4 == 0
    }
    
    func dump(_ path: URL) {
        data.write(to: path.appendingPathComponent(filename), atomically: true)
    }
}
