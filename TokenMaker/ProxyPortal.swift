//
//  ProxyPortal.swift
//  TokenMaker
//
//  Created by Eric Betts on 6/3/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Foundation

class ProxyPortal : PortalDelegate {
    var usbportal : USBPortal = USBPortal.singleton
    var bleportal : BLEPortal = BLEPortal.singleton
    var connected : Portal? = nil
    var delegate : PortalDelegate? = nil
    
    init() {
        //self.usbportal.discover()
        // self.usbportal.delegate = self
        self.bleportal.discover()
        self.bleportal.delegate = self
    }
    
    func deviceConnected(_ portal : Portal) {
        self.connected = portal
        guard let delegate = self.delegate else {
            print("No delegate for device connected")
            return
        }
        delegate.deviceConnected(portal)
    }
    
    func deviceDisconnected(_ portal : Portal) {
        self.connected = nil
        guard let delegate = self.delegate else {
            print("No delegate for device disconnected")
            return
        }
        delegate.deviceDisconnected(portal)
    }
    
    func input(_ report: Data) {
        guard let delegate = self.delegate else {
            print("No delegate for input")
            return
        }
        delegate.input(report)
    }
    
    func output(_ report : Data, delay : TimeInterval = 0.0) {
        guard self.connected != nil else {
            print("No device connected for sending output")
            return
        }
        
        if let usb = self.connected as? USBPortal {
            usb.output(report)
        } else if let ble = self.connected as? BLEPortal {
            ble.output(report)
        }
    }
}
