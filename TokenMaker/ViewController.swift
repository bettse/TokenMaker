//
//  ViewController.swift
//  TokenMaker
//
//  Created by Eric Betts on 5/30/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    @IBOutlet weak var libraryTable: NSTableView!
    @IBOutlet weak var seriesFilter: NSSegmentedControl!
    @IBOutlet weak var roleFilter: NSSegmentedControl!
    
    let portalDriver : PortalDriver = PortalDriver.singleton
    let allModels : [Model] = ThePoster.models
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let seriesList = Set(allModels.map { $0.series }).sorted(by: { $0.rawValue < $1.rawValue })
        
        seriesFilter.segmentCount = seriesList.count
        for series in seriesList {
            seriesFilter.setLabel(series.description, forSegment: Int(series.rawValue))
            seriesFilter.setSelected(true, forSegment: Int(series.rawValue))
        }
    }
    
    @IBAction func build(_ sender: AnyObject?) {
        if libraryTable.selectedRow != -1 {
            let model : Model = allModels[libraryTable.selectedRow]
            portalDriver.build(model)
        }
    }
    
    @IBAction func filterChange(_ sender: AnyObject?) {
        print("toggled: \(seriesFilter.selectedSegment)")
    }
}

// MARK: - NSTableViewDataSource
extension ViewController: NSTableViewDataSource {
    @objc(tableView:viewForTableColumn:row:) func tableView(_ tableView: NSTableView, viewFor viewForTableColumn: NSTableColumn?, row: Int) -> NSView? {
        let model : Model = allModels[row]
        let cell : NSTableCellView = tableView.makeView(withIdentifier: viewForTableColumn!.identifier, owner: self) as! NSTableCellView
        cell.textField?.stringValue = "\(model.name) - (\(model.role))"
        return cell
    }
}

extension ViewController: NSTableViewDelegate {
    @objc(numberOfRowsInTableView:) func numberOfRows(in tableView: NSTableView) -> Int {
        return allModels.count
    }
}
