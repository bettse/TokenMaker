//
//  SkylanderToken.swift
//  TokenMaker
//
//  Created by Eric Betts on 5/30/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Foundation
class SkylanderToken : MifareClassic1K {
    override var filename : String {
        get {
            return "\(uid.toHex)-\(name).bin"
        }
    }
    
    //Also known as 'ID' or 'type'.  I wanted to avoid those terms because of their alternate meanings
    var modelId : UInt16 {
        get {
            return block(1).subdata(in: 0..<2).uint16
        }
        set(newModelId) {
            //Assume this is happening on a zero'd out new token
            var m = newModelId
            var newBlock = block(1)
            newBlock.replaceSubrange(0..<2, with: Data(buffer: UnsafeBufferPointer(start: &m, count: 1)))
            load(1, blockData: newBlock)
            updateCrc()
        }
    }
    
    var flags : UInt16 {
        get {
            return block(1).subdata(in: 12..<14).uint16
        }
        set (newFlags) {
            //Assume this is happening on a zero'd out new token
            var m = newFlags
            var newBlock = block(1)
            newBlock.replaceSubrange(12..<14, with: Data(buffer: UnsafeBufferPointer(start: &m, count: 1)))
            load(1, blockData: newBlock)
            updateCrc()
        }
    }
    
    var model : Model {
        get {
            return Model(id: UInt(modelId), flags: flags)
        }
        set (newModel) {
            self.modelId = UInt16(newModel.id)
            self.flags = newModel.flags
        }
    }
    
    var name : String {
        get {
            return model.name
        }
    }
    
    var role : Role {
        get {
            return model.role
        }
    }
    
    var series : Series {
        get {
            return model.series
        }
    }
    
    func sectorTrailer(_ sectorNumber: Int) -> Data {
        var base : NSMutableData
        if (sectorNumber == 0) {
            base = (self.ro_sector as NSData).mutableCopy() as! NSMutableData
        } else {
            base = (self.rw_sector as NSData).mutableCopy() as! NSMutableData
        }
        
        let key = keyA(sectorNumber)
        base.replaceBytes(in: NSMakeRange(0, key.count), withBytes: (key as NSData).bytes)
        return base as Data
    }
    
    func crc64_like(_ initial: UInt64, input: UInt8) -> UInt64 {
        var crc : UInt64 = initial
        let poly : UInt64 = 0x42f0e1eba9ea3693
        let top : UInt64 = 0x800000000000
        
        crc = crc ^ (UInt64(input) << 40)
        for _ in (0..<8) {
            crc = ((crc & top) > 0) ? (crc << 1) ^ poly : crc << 1
        }
        
        return crc
    }
    
    func keyA(_ sectorNumber: Int) -> Data {
        if (sectorNumber == 0) {
            return Data(bytes: UnsafePointer<UInt8>([0x4b, 0x0b, 0x20, 0x10, 0x7c, 0xcb] as [UInt8]), count: MifareClassic1K.keyLength)
        } else {
            
            var crc : UInt64 = 0x9AE903260CC4
            crc = crc64_like(crc, input: uid[0])
            crc = crc64_like(crc, input: uid[1])
            crc = crc64_like(crc, input: uid[2])
            crc = crc64_like(crc, input: uid[3])
            crc = crc64_like(crc, input: UInt8(sectorNumber))
            return Data(buffer: UnsafeBufferPointer(start: &crc, count: MifareClassic1K.keyLength))
        }
    }
    
    func updateCrc() {
        var block1 = block(1).subdata(in: 0..<14)
        let special = NSMutableData()
        special.append(block(0))
        special.append(block1)
        block1.append((special as Data).crcCCITT)
        load(1, blockData: block1)
    }
    
    static func buildBlank(_ block0: Data) -> SkylanderToken {
        let uid = block0.subdata(in: 0..<4)
        let token = SkylanderToken(uid: uid)
        (1..<MifareClassic1K.blockCount).forEach { (blockNumber) in
            if (blockNumber % 4 == 3) {
                token.load(blockNumber, blockData: token.sectorTrailer(blockNumber/MifareClassic1K.sectorSize))
            } else {
                token.load(blockNumber, blockData: self.emptyBlock)
            }
        }
        return token
    }
}
